FROM node:latest

RUN set -eux; \
    apt-get update; \
    apt-get -y install ffmpeg

RUN set -eux; \
    curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl; \
    chmod a+rx /usr/local/bin/youtube-dl
